import math
import typing


class Ackerman:
    """Class that simulates the Ackerman steering model for a robot.

    Attributes:
        length (float): Length of the robot
        width (float): Width of the car
        xArray (list): List of x positions
        yArray (list): List of y positions
        thetaArray (list): List of theta positions
        xDotArray (list): List of x velocities
        yDotArray (list): List of y velocities
        thetaDotArray (list): List of angular velocities
        time (list): List of time values in seconds
    """

    def __init__(
        self,
        length: float,
        width: float,
        x_0: float = 0,
        y_0: float = 0,
        theta_0: float = 0,
        xDot_0: float = 0,
        yDot_0: float = 0,
        thetaDot_0: float = 0,
    ):
        """Initialize.

        Args:
            length (float): Length of the robot
            width (float): Width of the robot
            x_0 (float, optional): the initial x position. Defaults to 0.
            y_0 (float, optional): the initial y position. Defaults to 0.
            theta_0 (float, optional): the intial orientation from the y axis. 
                Defaults to 0.
            xDot_0 (float, optional): the initial x velocity. Defaults to 0.
            yDot_0 (float, optional): the initial y velocity. Defaults to 0.
            thetaDot_0 (float, optional): the initial angular velocity. 
                Defaults to 0.
        """
        self.__width = width
        self.__length = length
        self.__xArray: typing.List[float] = [x_0]
        self.__yArray: typing.List[float] = [y_0]
        self.__thetaArray: typing.List[float] = [theta_0]
        self.__xDotArray: typing.List[float] = [xDot_0]
        self.__yDotArray: typing.List[float] = [yDot_0]
        self.__thetaDotArray: typing.List[float] = [thetaDot_0]
        self.__time: typing.List[float] = [0]
        self.__realXArray = [x_0]
        self.__realYArray = [y_0]

    def breakIntoSteps(
        self, v_wheels: float, alpha: float, dt: float, time: float, uncertain: bool
    ) -> None:
        """Break the motion from a larger time step into the smaller steps."""
        count = 0.0
        v_Uncertain = (1 - 0.04) if uncertain else 1
        theta_Uncertain = (1 - 0.08) if uncertain else 1

        while count < time:
            self.__motion(
                v_wheels * v_Uncertain,
                alpha,
                self.__xArray[-1],
                self.__yArray[-1],
                self.__thetaArray[-1] * theta_Uncertain,
                dt,
            )
            self.__realMotion(
                v_wheels,
                alpha,
                self.__realXArray[-1],
                self.__realYArray[-1],
                self.__thetaArray[-1],
                dt,
            )
            count += dt

    def turn(self, v_wheels: float, radius: float, dt: float, time: float) -> None:
        """Implements a turn."""
        alpha = math.atan(self.__length / radius)
        self.breakIntoSteps(v_wheels, alpha, dt, time)

    def __motion(
        self, v_wheels: float, alpha: float, x: float, y: float, theta: float, dt: float
    ) -> typing.Tuple[float, float, float]:
        """Execute the forward kiniematics of the Ackerman steering model."""
        x_Dot = float(-v_wheels * math.sin(theta))
        y_Dot = float(v_wheels * math.cos(theta))
        theta_Dot = float((v_wheels / self.__length) * math.tan(alpha))

        x_new = x + x_Dot * dt
        y_new = y + y_Dot * dt
        theta_new = theta + theta_Dot * dt

        self.__updatePlots(x_new, y_new, theta_new, x_Dot, y_Dot, theta_Dot, dt)

        return (x_new, y_new, theta_new)

    def __realMotion(
        self, v_wheels: float, alpha: float, x: float, y: float, theta: float, dt: float
    ) -> None:
        """Execute the forward kinematics for the Ackerman model using continuous equations"""
        radius = self.__length / math.tan(alpha)
        angleOfTurnCircle = (v_wheels * dt) / radius

        # Calculating the true path of the bot if it is at 0,0
        x_new = radius * (1 - math.cos(angleOfTurnCircle))
        y_new = radius * math.sin(angleOfTurnCircle)

        # Using the matrix equation
        #  [x]    [cos(theta)  -sin(theta)]  [x_prime]
        #  | | =  |                       |  |       |
        #  [y]    [sin(theta)  cos(theta) ]  [y_prime]
        # to rotate the x_prime and y_prime coordinates to the world frame

        transformedX = x_new * math.cos(theta) - y_new * math.sin(theta)
        transformedY = x_new * math.sin(theta) + y_new * math.cos(theta)

        self.__realXArray.append(x + transformedX)
        self.__realYArray.append(y + transformedY)

    def __updatePlots(
        self,
        x: float,
        y: float,
        theta: float,
        x_Dot: float,
        y_Dot: float,
        theta_Dot: float,
        dt: float,
    ) -> None:
        """Update all array stored in class."""
        self.__xDotArray.append(x_Dot)
        self.__yDotArray.append(y_Dot)
        self.__thetaDotArray.append(theta_Dot)
        self.__xArray.append(x)
        self.__yArray.append(y)
        self.__thetaArray.append(theta)
        self.__time.append(self.__time[-1] + dt)

    def _getPositionArrays(
        self
    ) -> typing.Tuple[
        typing.List[float], typing.List[float], typing.List[float], typing.List[float]
    ]:
        return (self.__xArray, self.__yArray, self.__realXArray, self.__realYArray)

    def _getError(self) -> typing.Tuple[typing.List[float], typing.List[float]]:
        errorArray = [0]

        for i in range(1, len(self.__realXArray)):
            error = math.sqrt(
                (self.__realXArray[i] - self.__xArray[i]) ** 2
                + (self.__realYArray[i] - self.__yArray[i]) ** 2
            )
            errorArray.append(error)

        return (errorArray, self.__time)

    def _getVelocities(self) -> typing.Tuple[typing.List[float],typing.List[float],typing.List[float],typing.List[float]]:
        return (self.__xDotArray, self.__yDotArray, self.__thetaDotArray, self.__time)