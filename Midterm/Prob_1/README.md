# Problem 1

## Run Instructions

- Run the following command within this folder

  - ```bash
    python3 main.py
    ```

## Implementation explanation

- The dynamic models for both skidsteer and akerman are containted within their respective files.
- These files are then imported and used in the main file.

## Results

### a)
![Prob_1_Fig_1](../Figures/Problem_1_Figure_1.png)
![Prob_1_Fig_1](../Figures/Problem_1_Figure_2.png)
![Prob_1_Fig_1](../Figures/Problem_1_Figure_3.png)
![Prob_1_Fig_1](../Figures/Problem_1_Figure_4.png)

### b)
![Prob_2_Fig_1](../Figures/Problem_2_Figure_1.png)
![Prob_2_Fig_1](../Figures/Problem_2_Figure_2.png)
![Prob_2_Fig_1](../Figures/Problem_2_Figure_3.png)
![Prob_2_Fig_1](../Figures/Problem_2_Figure_4.png)


### c)
![Prob_3_Fig_1](../Figures/Problem_3_Figure_1.png)
![Prob_3_Fig_2](../Figures/Problem_3_Figure_2.png)
![Prob_3_Fig_3](../Figures/Problem_3_Figure_3.png)

### d)
![Prob_4_Fig_1](../Figures/Problem_4_Figure_1.png)
![Prob_4_Fig_2](../Figures/Problem_4_Figure_2.png)
![Prob_4_Fig_3](../Figures/Problem_4_Figure_3.png)
