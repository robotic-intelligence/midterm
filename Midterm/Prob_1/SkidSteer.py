import math
import typing


class SkidSteer:
    """Class to represent a skid steer robot

    Attributes:
        length (float): The length of the robot
        width (float): The width of the robot
        xArray (list): The x position of the robot at each time step
        yArray (list): The y position of the robot at each time step
        thetaArray (list): The orientation of the robot at each time step
        xDotArray (list): The x velocity of the robot at each time step
        yDotArray (list): The y velocity of the robot at each time step
        thetaDotArray (list): The angular velocity of the robot at each time 
            step
        time (list): The time in seconds at each time step
    """

    def __init__(
        self,
        length: float,
        width: float,
        x_0: float = 0,
        y_0: float = 0,
        theta_0: float = 0,
        xDot_0: float = 0,
        yDot_0: float = 0,
        thetaDot_0: float = 0,
    ):
        """Initialize.

        Args:
            length (float): The length of the robot
            width (float): The width of the robot
            x_0 (float, optional): The initial x position of the robot. 
                Defaults to 0.
            y_0 (float, optional): The initial y position of the robot.
                Defaults to 0.
            theta_0 (float, optional): The initial orientation of the robot.
                Defaults to 0.
            xDot_0 (float, optional): The initial x velocity of the robot.
                Defaults to 0.
            yDot_0 (float, optional): The initial y velocity of the robot.
                Defaults to 0.
            thetaDot_0 (float, optional): The initial angular velocity of the 
                robot. Defaults to 0.
        """
        self.__width = width
        self.__length = length
        self.__xArray: typing.List[float] = [x_0]
        self.__yArray: typing.List[float] = [y_0]
        self.__thetaArray: typing.List[float] = [theta_0]
        self.__xDotArray: typing.List[float] = [xDot_0]
        self.__yDotArray: typing.List[float] = [yDot_0]
        self.__thetaDotArray: typing.List[float] = [thetaDot_0]
        self.__time: typing.List[float] = [0]

    def breakIntoSteps(
        self, v_left: float, v_right: float, dt: float, time: float
    ) -> None:
        """Breaks the motion of a longer command into smaller steps"""

        count = 0.0
        while count < time:
            self.__motion(
                v_left,
                v_right,
                self.__xArray[-1],
                self.__yArray[-1],
                self.__thetaArray[-1],
                dt,
            )
            count += dt

    def turn(self, angle: float, maxVel: float, velStep: float, dt: float) -> None:

        bestTurnVel = 0.0
        bestTurnTime = 0.0
        bestError = math.inf
        i = dt
        while i <= maxVel:
            thetaDot = (i + i) / self.__width
            actualTime = math.radians(angle) / thetaDot
            realTime = (actualTime // dt) * dt
            error = abs(actualTime - realTime) * thetaDot
            if error < bestError:
                bestError = error
                bestTurnVel = i
                bestTurnTime = realTime
            i += velStep

        if angle < 0:
            self.breakIntoSteps(bestTurnVel, -bestTurnVel, dt, bestTurnTime)
        else:
            self.breakIntoSteps(-bestTurnVel, bestTurnVel, dt, bestTurnTime)

    def __motion(
        self, v_left: float, v_right: float, x: float, y: float, theta: float, dt: float
    ) -> typing.Tuple[float, float, float]:
        """Calculate the forward kinematics of the robot"""
        aveVel = (1 / 2) * (v_right + v_left)
        x_Dot = -aveVel * math.sin(theta)
        y_Dot = aveVel * math.cos(theta)
        theta_Dot = (v_right - v_left) / self.__width

        x_new = x + x_Dot * dt
        y_new = y + y_Dot * dt
        theta_new = theta + theta_Dot * dt

        self.__updatePlots(x_new, y_new, theta_new, x_Dot, y_Dot, theta_Dot, dt)

        return x_new, y_new, theta_new

    def __updatePlots(
        self,
        x: float,
        y: float,
        theta: float,
        x_Dot: float,
        y_Dot: float,
        theta_Dot: float,
        dt: float,
    ) -> None:
        """Add the new values to the arrays"""
        self.__xDotArray.append(x_Dot)
        self.__yDotArray.append(y_Dot)
        self.__thetaDotArray.append(theta_Dot)
        self.__xArray.append(x)
        self.__yArray.append(y)
        self.__thetaArray.append(theta)
        self.__time.append(self.__time[-1] + dt)

    def _getPositionArrays(
        self
    ) -> typing.Tuple[typing.List[float], typing.List[float]]:
        return (self.__xArray, self.__yArray)

    def _getVelocities(self) -> typing.Tuple[typing.List[float],typing.List[float],typing.List[float],typing.List[float]]:
        return (self.__xDotArray, self.__yDotArray, self.__thetaDotArray, self.__time)