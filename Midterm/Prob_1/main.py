import math
import os.path
import typing

import matplotlib.pyplot as plt

from Midterm.Prob_1.Ackerman import Ackerman
from Midterm.Prob_1.SkidSteer import SkidSteer


def skidSteerCircle(skidSteer: SkidSteer, dt: float) -> None:
    skidSteer.breakIntoSteps(6.25, 9.75, dt, 0.1)
    skidSteer.breakIntoSteps(6.25, 9.75, dt, 0.1)
    skidSteer.breakIntoSteps(6.25, 9.75, dt, 0.1)
    skidSteer.breakIntoSteps(5.9, 10.1, dt, 0.1)
    skidSteer.breakIntoSteps(7.05, 8.95, dt, 0.1)

    for i in range(100):
        skidSteer.breakIntoSteps(7.1, 8.9, dt, dt)


def ackermanCircleFromCenter(ackerman: Ackerman, dt: float, uncertain: bool) -> None:

    ackerman.breakIntoSteps(8, 0.6, dt, 0.1, uncertain)
    ackerman.breakIntoSteps(8, 0.6, dt, 0.1, uncertain)
    ackerman.breakIntoSteps(8, 0.5, dt, 0.1, uncertain)
    ackerman.breakIntoSteps(8, 0.547, dt, 0.1, uncertain)
    ackerman.breakIntoSteps(8, 0.40, dt, 0.1, uncertain)

    ackermanCircleFromEdge(ackerman, dt, uncertain)


def ackermanCircleFromEdge(ackerman: Ackerman, dt: float, uncertain: bool):
    i = 0
    while i < 10:
        ackerman.breakIntoSteps(8, 0.303, dt, 0.1, uncertain)
        i += dt


def createPlot(
    xArray: typing.List[float],
    yArray: typing.List[float],
    title,
    probNumber: int,
    figNumber: int,
    circleDia: float = 0,
):
    dirName = os.path.dirname(__file__)

    if circleDia > 0:
        figure, axes = plt.subplots()
        axes.set(xlim=(-3, 3), ylim=(-3, 3))
        axes.set_aspect("equal")
        circle = plt.Circle((0.0, 0.0), circleDia / 2, fill=False)
        axes.add_artist(circle)
    else:
        plt.figure()
    plt.title(title)
    plt.plot(xArray, yArray)
    plt.savefig(
        os.path.join(dirName, f"../Figures/Problem_{probNumber}_Figure_{figNumber}.png")
    )


if __name__ == "__main__":
    length = 0.75
    width = 0.55
    diameter = 5
    vel = 8
    dt = 0.1

    # getting path to file
    dirName = os.path.dirname(__file__)

    # A. Skid Steer
    skidSteer = SkidSteer(length, width)
    skidSteerCircle(skidSteer, dt)
    x_Skid, y_Skid = skidSteer._getPositionArrays()
    xdot_Skid, ydot_Skid, thetadot_Skid, time_Skid = skidSteer._getVelocities()
    createPlot(x_Skid, y_Skid, "Skid Steer", 1, 1, diameter)
    createPlot(time_Skid, xdot_Skid, "Skid Steer xdot", 1, 2)
    createPlot(time_Skid, ydot_Skid, "Skid Steer ydot", 1, 3)
    createPlot(time_Skid, thetadot_Skid, "Skid Steer thetadot", 1, 4)


    # B. Ackerman Steer
    ackerman = Ackerman(length, width)
    ackermanCircleFromCenter(ackerman, dt, False)
    x_Ack, y_Ack, x_real, y_real = ackerman._getPositionArrays()
    xdot_Ack, ydot_Ack, thetadot_Ack, time_Ack = ackerman._getVelocities()
    createPlot(x_Ack, y_Ack, "Ackerman Steer", 2, 1, diameter)
    createPlot(time_Ack, xdot_Ack, "Ackerman Steer xdot", 2, 2)
    createPlot(time_Ack, ydot_Ack, "Ackerman Steer ydot", 2, 3)
    createPlot(time_Ack, thetadot_Ack, "Ackerman Steer thetadot", 2, 4)

    # C. Ackerman Steer Error #######################################

    # dt = 1
    ackerman1 = Ackerman(length, width)
    ackermanCircleFromEdge(ackerman1, 1, False)
    error1, time1 = ackerman1._getError()
    createPlot(time1, error1, "Ackerman Steer Error dt = 1", 3, 1)

    # dt = .1
    ackerman2 = Ackerman(length, width)
    ackermanCircleFromEdge(ackerman2, 0.1, False)
    error2, time2 = ackerman2._getError()
    createPlot(time2, error2, "Ackerman Steer Error dt = 0.1", 3, 2)

    # dt = .01
    ackerman3 = Ackerman(length, width)
    ackermanCircleFromEdge(ackerman3, 0.01, False)
    error3, time3 = ackerman3._getError()
    createPlot(time3, error3, "Ackerman Steer Error dt = 0.01", 3, 3)

    # D Ackerman Steer Uncertain ####################################
    # dt = 1
    ackerman4 = Ackerman(length, width)
    ackermanCircleFromEdge(ackerman4, 1, True)
    error1, time1 = ackerman4._getError()
    createPlot(time1, error1, "Uncertain Ackerman Steer Error dt = 1", 4, 1)

    # dt = .1
    ackerman5 = Ackerman(length, width)
    ackermanCircleFromEdge(ackerman5, 0.1, True)
    error2, time2 = ackerman5._getError()
    createPlot(time2, error2, "Uncertain Ackerman Steer Error dt = 0.1", 4, 2)

    # dt = .01
    ackerman6 = Ackerman(length, width)
    ackermanCircleFromEdge(ackerman6, 0.01, True)
    error3, time3 = ackerman6._getError()
    createPlot(time3, error3, "Uncertain Ackerman Steer Error dt = 0.01", 4, 3)

    plt.show()
