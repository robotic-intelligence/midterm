import numpy as np


def main():

    # parameters
    a1 = 60
    a2 = 40
    theta_1 = 30
    theta_2 = 45
    theta_3 = 90
    d = 14

    d_h = np.array(
        [[60 , 0, 0, theta_1],
         [40, 180, 0, theta_2],
         [0, 0, d, theta_3],
         [0, 0, 1, 0]])


    a1 = np.array(
        [
            [
                np.cos(theta_1),
                -np.sin(theta_1),
                0,
                a1 * np.cos(theta_1),
            ],
            [
                np.sin(theta_1),
                np.cos(theta_1),
                0,
                a1 * np.sin(theta_1),
            ],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ]
    )

    a2 = np.array(
        [
            [
                np.cos(theta_2),
                np.sin(theta_2),
                0,
                a2 * np.cos(theta_2),
            ],
            [
                np.sin(theta_2),
                -np.cos(theta_2),
                0,
                a2 * np.sin(theta_2),
            ],
            [0, 0, -1, 0],
            [0, 0, 0, 1]
        ]
    )

    a3 = np.array(
        [
            [
                np.cos(theta_3),
                -np.sin(theta_3),
                0,
                0
            ],
            [
                np.sin(theta_3),
                np.cos(theta_3),
                0,
                0
            ],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ]
    )

    a4 = np.array(
        [
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 1, d],
            [0, 0, 0, 1]
        ]
    )

    solution = a1 @ a2 @ a3 @ a4

    print("Forward Kinematics: ")
    print(solution)

if __name__ == "__main__":
    main()
