## Problem 3

# Run Instructions

- Run the following command within this folder

  - ```bash
      python3 prob_3.py
      ```

- The output will be displayed in the terminal
- It will display the final state matrix and the final command vector
![Problem_3](sol_3.png)
