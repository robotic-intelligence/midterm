"""File that solves problem 3 on midterm."""
import math
import random
import typing

import numpy as np
import numpy.typing as npt
from numpy import cos as c
from numpy import sin as s


def define_matrix(
    theta_1: float,
    theta_4: float,
    theta_5: float,
    theta_6: float,
    d_1: float,
    d_2: float,
    d_3: float,
    d_6: float,
) -> npt.NDArray[np.float64]:
    """Define the state matrix.
    
    Args:
        theta_1: theta 1 
        theta_4: theta 2 
        theta_5: theta 5
        theta_6: theta 6 
        d_1: d 1
        d_2: d 2  
        d_3: d 3 
        d_6: d 6 
        
    Returns:
        the current state matrix of the form 
            [[r_11, r_12, r_13, d_x],
             [r_21, r_22, r_23, d_y],
             [r_31, r_32, r_33, d_z],
             [   0,    0,    0,   1]]

    """
    # r_11
    r_11 = c(theta_1) * c(theta_4) * c(theta_5) * c(theta_6)
    r_11 -= c(theta_1) * s(theta_4) * s(theta_6)
    r_11 += s(theta_1) * s(theta_5) * c(theta_6)
    # r_21
    r_21 = s(theta_1) * c(theta_4) * c(theta_5) * c(theta_6)
    r_21 -= s(theta_1) * s(theta_4) * s(theta_6)
    r_21 -= c(theta_1) * s(theta_5) * c(theta_6)
    # r_31
    r_31 = -s(theta_4) * c(theta_5) * c(theta_6)
    r_31 -= c(theta_4) * s(theta_6)
    # r_12
    r_12 = -c(theta_1) * c(theta_4) * c(theta_5) * s(theta_6)
    r_12 -= c(theta_1) * s(theta_4) * c(theta_6)
    r_12 -= s(theta_1) * s(theta_5) * c(theta_6)
    # r_22
    r_22 = -s(theta_1) * c(theta_4) * c(theta_5) * s(theta_6)
    r_22 -= s(theta_1) * s(theta_4) * s(theta_6)
    r_22 += c(theta_1) * s(theta_5) * c(theta_6)
    # r_32
    r_32 = s(theta_4) * c(theta_5) * c(theta_6)
    r_32 -= c(theta_4) * c(theta_6)
    # r_13
    r_13 = c(theta_1) * c(theta_4) * s(theta_5)
    r_13 -= s(theta_1) * c(theta_5)
    # r_23
    r_23 = s(theta_1) * c(theta_4) * s(theta_5)
    r_23 += c(theta_1) * c(theta_5)
    # r_33
    r_33 = -s(theta_4) * s(theta_5)
    # d_x
    d_x = c(theta_1) * c(theta_4) * s(theta_5) * d_6
    d_x -= s(theta_1) * c(theta_5) * d_6
    d_x -= s(theta_1) * d_3
    # d_y
    d_y = s(theta_1) * c(theta_4) * s(theta_5) * d_6
    d_y += c(theta_1) * c(theta_5) * d_6
    d_y += c(theta_1) * d_3
    # d_z
    d_z = -s(theta_4) * s(theta_5) * d_6
    d_z += d_1 + d_2
    # define the state matrix
    matrix: npt.NDArray = np.array(
        [
            [r_11, r_12, r_13, d_x],
            [r_21, r_22, r_23, d_y],
            [r_31, r_32, r_33, d_z],
            [0, 0, 0, 1],
        ]
    )
    return matrix


def monte_carlo_arm_kinematics(
    x_goal: float,
    y_goal: float,
    z_goal: float,
    initial_conditions: typing.Tuple[
        float, float, float, float, float, float, float, float
    ],
) -> typing.Tuple[float, float, float, float, float]:
    """Compute angles for arm given goal points.

    Args:
        x_goal: goal location on the x axis
        y_goal: goal location of the y axis
        z_goal: goal location of the z axis
        initial_conditions: the initial conditions for theta_1, theta_4,
            theta_5, theta_6, d_1, d_2, d_3, d_6. 

    Returns:
        the optimal solution for the positions of theta_1, theta_4, theta_5, 
            d_2, d_3 given 1000 iterations.

    """
    # Initial conditions
    theta_1, theta_4, theta_5, theta_6, d_1, d_2, d_3, d_6 = initial_conditions
    # set the perturbation tolerance
    tolerance = 0.02
    # set the termination tolerance
    epsilon = 0.01
    # set scalar for to minimize expensive perturbations
    theta_1_pert_scalar = 1 / 3
    d_2_pert_scalar = 1 / 2
    d_3_pert_scalar = 1 / 2
    # define initial state matrix
    init_matrix = define_matrix(
        initial_conditions[0],
        initial_conditions[1],
        initial_conditions[2],
        initial_conditions[3],
        initial_conditions[4],
        initial_conditions[5],
        initial_conditions[6],
        initial_conditions[7],
    )
    # create array for solutions
    solutions: typing.List[typing.Tuple[float, float, float, float, float]] = []
    # create loop that runs n simulations
    for i in range(2000):
        # reset conditions to start new loop
        matrix = init_matrix
        prev_matrix = init_matrix
        theta_1, theta_4, theta_5, theta_6, d_1, d_2, d_3, d_6 = initial_conditions
        # set termination conditions
        while (
            _distance(x_goal, y_goal, z_goal, matrix[0][3], matrix[1][3], matrix[2][3])
            > epsilon
        ):
            # perturb values with randomness
            theta_1_diff = (random.random() - 0.5) * tolerance * theta_1_pert_scalar
            theta_4_diff = (random.random() - 0.5) * tolerance
            theta_5_diff = (random.random() - 0.5) * tolerance
            d_2_diff = (random.random() - 0.5) * tolerance * d_2_pert_scalar
            d_3_diff = (random.random() - 0.5) * tolerance * d_3_pert_scalar
            theta_1_temp = theta_1 + theta_1_diff
            theta_4_temp = theta_4 + theta_4_diff
            theta_5_temp = theta_5 + theta_5_diff
            d_2_temp = d_2 + d_2_diff
            d_3_temp = d_3 + d_3_diff
            # define matrix with new values
            matrix = define_matrix(
                theta_1_temp,
                theta_4_temp,
                theta_5_temp,
                theta_6,
                d_1,
                d_2_temp,
                d_3_temp,
                d_6,
            )
            # check to see if closer to goal
            if _distance(
                x_goal, y_goal, z_goal, matrix[0][3], matrix[1][3], matrix[2][3]
            ) < _distance(
                x_goal,
                y_goal,
                z_goal,
                prev_matrix[0][3],
                prev_matrix[1][3],
                prev_matrix[2][3],
            ):
                # if so save changes
                theta_1 = theta_1_temp
                theta_4 = theta_4_temp
                theta_5 = theta_5_temp
                d_2 = d_2_temp
                d_3 = d_3_temp
                prev_matrix = matrix
        solutions.append((theta_1, theta_4, theta_5, d_2, d_3))

    # create place holder for best solution and best cost
    best_sol = (0.0, 0.0, 0.0, 0.0, 0.0)
    best_cost = np.inf
    theta_1, theta_4, theta_5, theta_6, d_1, d_2, d_3, d_6 = initial_conditions
    for sol in solutions:
        # evaluate cost
        cost = (
            abs(theta_1 - sol[0]) * 1 / theta_1_pert_scalar
            + abs(theta_4 - sol[1])
            + abs(theta_5 - sol[2])
            + (abs(d_2 - sol[3]) + abs(d_3 - sol[4])) * 1 / d_2_pert_scalar
        )
        # if cost is less expensive change the solution and cost
        if cost < best_cost:
            best_cost = cost
            best_sol = sol
    return best_sol


def _distance(
    x_goal: float, y_goal: float, z_goal: float, d_x: float, d_y: float, d_z: float
) -> float:
    """Get the distance from current state to goal.

    Args:
        x_goal: x goal 
        y_goal: y goal 
        z_goal: z goal
        d_x: current x 
        d_y: current y
        d_z: current z

    Returns:
        the scalar value of the distance

    """
    return np.sqrt((x_goal - d_x) ** 2 + (y_goal - d_y) ** 2 + (z_goal - d_z) ** 2)


if __name__ == "__main__":
    goal_x, goal_y, goal_z = 1.2, 0.8, 0.5
    initial_cond = (
        float(-np.pi / 2),
        float(-np.pi / 2),
        float(np.pi / 2),
        float(40 * np.pi / 180),
        0.5,
        0.5,
        1,
        0.2,
    )
    sol = monte_carlo_arm_kinematics(goal_x, goal_y, goal_z, initial_cond)
    matrix = define_matrix(
        sol[0], sol[1], sol[2], float(40 * np.pi / 180), 0.5, sol[3], sol[4], 0.2
    )
    print(f"Goal: {goal_x}, {goal_y}, {goal_z}")
    print(f"Solution: {sol}")
    print(f"Matrix: \n{matrix}")
