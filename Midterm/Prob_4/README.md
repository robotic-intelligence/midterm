# Problem 4

## Part A

- As the ball accelerates more as the angle grows it becomes more difficult to stabilize as the the angle grows.
- As the mass of the poll and the length of the pole grow the system becomes harder to stabilize.

- $x$ double dot is affected by the centrifugal force in the ball as theta grows there is more of an x component.
            The angular acceleration will always appose the motion if recoverable.

## Part B

- We genereated two solutions for this problem. For the first we generated a huristic that attempts to solve for the force some perturbated $\theta$ and contiues to push the the mass closer to 0. This solution was marginally stable and created ossilation as $\theta$ began to converge on zero.
  - to run this solution run the following command for this subfolder

    ```bash
            python3 cart_pole.py
    ```

- The second solution utilizes LQR to solve for the optimal control input. This solution was able to stabilize the system and keep it at 0 for the entire time.
  - to run this solution run the following command for this subfolder

   ```bash
            python3 cart_pole_lqr.py
    ```

## Part C

$$0 = \frac{g \sin{\theta}- \frac{\cos{\theta}F}{M_{\text{system}}}}{L(\frac{4}{3}- \frac{m_p \cos{\theta}^2}{M_{\text{system}}})}$$

$$g \sin{\theta} = \frac{\cos{\theta} F}{M_{\text{system}}}$$

$$\frac{\sin{\theta}}{\cos{\theta}} = \frac{F}{Mg}$$

$$\tan{\theta} = \frac{F}{Mg} $$

$$\theta = 8.285 \deg$$

### Figures for Huristic Solution

![Figure_1](./fig/Figure_1.png)
![Figure_2](./fig/Figure_2.png)
![Figure_3](./fig/Figure_3.png)

### Video For LQR Solution

[![](http://img.youtube.com/vi/NIDSLH__M_A/0.jpg)](http://www.youtube.com/watch?v=NIDSLH__M_AkTNfi5z6Uvk "LQR")
