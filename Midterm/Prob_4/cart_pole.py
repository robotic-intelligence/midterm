from random import random
import typing

import matplotlib.pyplot as plt
import numpy
from numpy import cos as c
from numpy import sin as s


class CartPole:
    """Class that simulates a cart pole system.

    Attributes:
        l (float): length of the pole
        m_p (float): mass of the pole
        m_c (float): mass of the cart
        g (float): acceleration due to gravity

    """

    def __init__(self) -> None:
        """Initialize.

        Args:
            None
        """
        # mass of cart
        self.m_c = 4
        # mass of pole
        self.m_p = 0.2
        # length of pole
        self.l = 1
        # gravity
        self.g = 9.81
        # initial theta terms
        self.theta = numpy.pi / 15
        self.theta_dot = 0.0
        self.theta_dot_dot = 0.0
        # initial x terms
        self.x = 0.0
        self.x_dot = 0.0
        self.x_dot_dot = 0.0
        # min time step before numerical instability
        self.time_step = 0.082

    def get_kinematics(self, force: float) -> None:
        """Get the kinematics of the cartpole system.

        Args:
            force: The force applied to the cart for the time-step.

        """
        # setting terms of readability for theta" equation
        first_term: float = self.g * s(self.theta)
        second_term: float = c(self.theta) * (
            -force - self.m_p * self.l * self.theta_dot ** 2 * s(self.theta)
        )
        second_term /= self.m_c + self.m_p
        third_term = self.l * (
            4 / 3 - self.m_p * c(self.theta) ** 2 / (self.m_c + self.m_p)
        )
        self.theta_dot_dot = (first_term + second_term) / third_term
        # setting terms of readability for x" equation
        top = force + self.m_p * self.l * (
            self.theta_dot ** 2 * s(self.theta) - self.theta_dot_dot * c(self.theta)
        )
        self.x_dot_dot = top / (self.m_c + self.m_p)
        # setting velocity terms
        self.theta_dot += self.theta_dot_dot * self.time_step
        self.x_dot += self.x_dot_dot * self.time_step
        # update theta
        self.theta += self.theta_dot * self.time_step
        self.x += (
            1 / 2 * self.x_dot_dot ** 2 * self.time_step + self.x_dot * self.time_step
        )

    def get_force(self) -> float:
        """Get the force applied to the cart.

        Args:
            None

        Returns:
            float: force applied to the cart

        """
        # set the perturbation of the angle
        theta_perturb = (numpy.pi / 180) * 0.1
        if self.theta < 0:
            # if angle is negative perturb it in the positive direction
            theta_goal = self.theta + theta_perturb
            # if perturbation crosses the zero line, set the goal to zero
            if theta_goal > 0:
                theta_goal = 0
        else:
            # if angle is positive perturb it in the negative direction
            theta_goal = self.theta - theta_perturb
            # if perturbation crosses the zero line, set the goal to zero
            if theta_goal < 0:
                theta_goal = 0
        # solve for theta" for euler integration
        self.theta_dot_dot = (
            2 * (theta_goal - self.theta - self.theta_dot * self.time_step) / self.time_step ** 2
        )
        # cody's readability term
        cody_separation = 4 / 3 - self.m_p * c(self.theta) ** 2 / (self.m_c + self.m_p)
        # the rest of the readability terms
        first_term = self.m_p * self.l * self.theta_dot ** 2 * s(self.theta)
        second_term = self.m_p + self.m_c
        second_term *= self.theta_dot_dot * self.l * cody_separation - self.g * s(
            self.theta
        )
        second_term /= cody_separation
        # return the force
        force = -first_term - second_term
        return force

    def control_loop(self) -> None:
        """Control loop for the cart-pole system.

        Args:
            None
        """
        # setting initial force
        force = 0.0
        # setting arrays for saving terms
        self.x_list: typing.List[float] = []
        self.theta_list: typing.List[float] = []
        self.x_dot_list: typing.List[float] = []
        self.theta_dot_list: typing.List[float] = []
        self.force_list: typing.List[float] = []
        self.time_list: typing.List[float] = []
        for i in range(1000):
            # append terms to lists
            self.x_list.append(self.x)
            self.theta_list.append(self.theta*180/numpy.pi)
            self.force_list.append(force)
            self.time_list.append(i * self.time_step)
            # get force for iteration
            force = self.get_force()
            # update kinematics
            self.get_kinematics(force)
            # set edge conditions for break
            if self.theta == 0 and self.theta_dot == 0 and self.theta_dot_dot == 0:
                break
        print(f"Time to complete: {i*self.time_step}")
        print(f"Final x: {self.x}")
        print(f"Final theta: {self.theta}")
        self._plot_results()

    def _plot_results(self) -> None:
        plt.plot(self.time_list, self.theta_list, c="r", label="theta")
        plt.xlabel("Time (s)")
        plt.ylabel("Angle (deg)")
        _, ax_1 = plt.subplots()
        ax_1.plot(self.time_list, self.force_list, c="g", label="force")
        ax_1.set_xlabel("Time (s)")
        ax_1.set_ylabel("force")
        _, ax_2 = plt.subplots()
        ax_2.plot(self.time_list, self.x_list, c="b", label="x")
        ax_2.set_xlabel("Time (s)")
        ax_2.set_ylabel("x pos (m)")
        plt.legend()
        ax_1.legend()
        ax_2.legend()
        plt.show()


if __name__ == "__main__":
    cart_pole = CartPole()
    cart_pole.control_loop()
