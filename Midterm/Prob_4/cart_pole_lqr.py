# credit to https://towardsdatascience.com/comparing-optimal-control-and-reinforcement-learning-using-the-cart-pole-swing-up-openai-gym-772636bc48f4 for helping me solve this problem
import numpy as np
from numpy import sin, cos, pi
from scipy import linalg
import matplotlib.pyplot as plt
import gym


def apply_state_controller(K, x):
    print(K)
    print(x)
    u = -np.dot(K, x)
    if u > 0:
        return 1, u
    else:
        return 0, u


def main():
    # theta = pi / 4
    m_c =  4
    m_p = 0.2
    l_p = 1
    g = 9.81
    a = g/(l_p*(4.0/3 - m_p/(m_p+m_c)))
    A = np.array([[0, 1, 0, 0],
                  [0, 0, a, 0],
                  [0, 0, 0, 1],
                  [0, 0, a, 0]])
    b = -1/(l_p*(4.0/3 - m_p/(m_p+m_c)))
    B = np.array([[0], [1/(m_p+m_c)], [0], [b]])
    R = np.eye(1)
    print(R)
    Q = 5*np.eye(4)
    P = linalg.solve_continuous_are(A, B, Q, R)
    first = np.linalg.inv(R)
    second = np.dot(B.T, P)
    K = np.dot(first, second)
    env = gym.make('CartPole-v0')
    env.env.seed(1)
    obs = env.reset()
    for i in range(1000):
        env.render()
        action, force = apply_state_controller(K,obs) 

        abs_force = abs(float(np.clip(force, -10, 10)))
        env.env.force_mag = abs_force
        
        obs, reward, done, _ = env.step(action)
        if done:
            break
    env.close()

if __name__ == '__main__':
    main()



