import random
import matplotlib.pyplot as plt
from IPython.display import clear_output
import numpy as np
import time
from Midterm.Prob_5.DQN import DQN
import torch

class CartPoleDQN(DQN):

    def replay(self, memory, size, gamma=0.9):
        """New replay function"""
        # Try to improve replay speed
        if len(memory) >= size:
            batch = random.sample(memory, size)
            batch_t = list(map(list, zip(*batch)))  # Transpose batch list
            states = batch_t[0]
            actions = batch_t[1]
            next_states = batch_t[2]
            rewards = batch_t[3]
            is_dones = batch_t[4]

            states = torch.Tensor(np.array(states))
            actions_tensor = torch.Tensor(actions)
            next_states = torch.Tensor(np.array(next_states))
            rewards = torch.Tensor(rewards)
            is_dones_tensor = torch.Tensor(is_dones)

            is_dones_indices = torch.where(is_dones_tensor == True)[0]

            all_q_values = self.model(states)  # predicted q_values of all states
            all_q_values_next = self.model(next_states)
            # Update q values

            all_q_values[range(len(all_q_values)), actions] = rewards + gamma * torch.max(all_q_values_next,axis=1).values
            isDone_list = [int(i) for i in actions_tensor[is_dones].tolist()]
            all_q_values[is_dones_indices.tolist(), isDone_list] = rewards[is_dones_indices.tolist()]

            self.update(states.tolist(), all_q_values.tolist())

    @staticmethod
    def q_learning(env, model, episodes, gamma=0.9,
                   epsilon=0.3, eps_decay=0.99,
                   replay=False, replay_size=20,
                   title='DQL', double=False,
                   n_update=10, soft=False, verbose=True):
        """Deep Q Learning algorithm using the DQN. """

        final = []
        memory = []
        episode_i = 0
        sum_total_replay_time = 0
        for episode in range(episodes):
            episode_i += 1
            if double and not soft:
                # Update target network every n_update steps
                if episode % n_update == 0:
                    model.target_update()
            if double and soft:
                model.target_update()

            # Reset state
            state = env.reset()[0]
            done = False
            total = 0

            while not done:
                # Implement greedy search policy to explore the state space
                if random.random() < epsilon:
                    action = env.action_space.sample()
                else:
                    q_values = model.predict(state)
                    action = torch.argmax(q_values).item()

                # Take action and add reward to total
                next_state, reward, done, _, _ = env.step(action)
                reward -= abs(next_state[2]) + abs(next_state[3])
                # Update total and memory
                total += reward
                memory.append((state, action, next_state, reward, done))
                q_values = model.predict(state).tolist()

                if done:
                    if not replay:
                        q_values[action] = reward
                        # Update network weights
                        model.update(state, q_values)
                    break

                if replay:
                    t0 = time.time()
                    # Update network weights using replay memory
                    model.replay(memory, replay_size, gamma)
                    t1 = time.time()
                    sum_total_replay_time += (t1 - t0)
                else:
                    # Update network weights using the last step only
                    q_values_next = model.predict(next_state)
                    q_values[action] = reward + gamma * torch.max(q_values_next).item()
                    model.update(state, q_values)

                state = next_state

            # Update epsilon
            epsilon = max(epsilon * eps_decay, 0.01)
            final.append(total)

            if verbose:
                print("episode: {}, total reward: {}".format(episode_i, total))
                if replay:
                    print("Average replay time:", sum_total_replay_time / episode_i)
        CartPoleDQN.plot_res(final, title)
        return final

    @staticmethod
    def plot_res( values, title=''):
        ''' Plot the reward curve and histogram of results over time.'''
        # Update the window after each episode
        clear_output(wait=True)

        # Define the figure
        f, ax = plt.subplots(nrows=1, ncols=2, figsize=(12, 5))
        f.suptitle(title)
        ax[0].plot(values, label='score per run')
        ax[0].axhline(195, c='red', ls='--', label='goal')
        ax[0].set_xlabel('Episodes')
        ax[0].set_ylabel('Reward')
        x = range(len(values))
        ax[0].legend()
        # Calculate the trend
        try:
            z = np.polyfit(x, values, 1)
            p = np.poly1d(z)
            ax[0].plot(x, p(x), "--", label='trend')
        except:
            print('')

        # Plot the histogram of results
        ax[1].hist(values[-50:])
        ax[1].axvline(195, c='red', label='goal')
        ax[1].set_xlabel('Scores per Last 50 Episodes')
        ax[1].set_ylabel('Frequency')
        ax[1].legend()
        plt.show()
