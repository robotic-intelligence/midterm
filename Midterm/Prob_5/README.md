# Problem 5

## Part A

- Reinforcement learning allows for a system to make optimal decisions with no information about the model.
- Reinforcement learning can get information from the environment by interacting with it.
- Reinforcement learning can learn from its previous mistakes and correct its interpretation of the model
- Reinforcement learning does not allow for skill transference. Meaning with full retraining under small perturbation on problem require retraining.
- Reinforcement learning is extremely computation heavy while training.
- Reinforcement learning needs a substantial amount of data to train, or it needs a real life model to acquire data.

## Part B

![Control vs Reinforcement](rein_vs_control.png)]

- In the controls model, the controller knows how to translate the state of the system to some input to the system. In the reinforcement model, necessary input to the system is being constantly refined to get a desired output.
- In the controls model, the model is of the system is limited to the knowledge of the person creating the controls. However, in the reinforcement model, the model can determine more information about the physical model as it goes.
- In the controls model, the goal for the controller is to achieve a desired state that minimizes error between the expected and actual output. In the reinforcement model, the agent carries out actions that maximize the amount of reward it gets. This allows the reinforcement model to act with multiple goals in mind.

## Part C

- For this problem I implemented Deep Q reinforcement learning to solve the discrete problem
- The graph below shows that modifying the reward to punish angular velocity of the pole makes the amount of reward accrued to rocket to nearly 2000 seconds.

![Results for cartpole](cartpole.png)

## Part D

- For this problem we used OpenAis spinning up ddpg alorighm implemented in pytorch. Found here <https://spinningup.openai.com/en/latest/algorithms/ddpg.html>.
- to display a new run of the saved model run the following script from the root of the project
- to run this script you must have mujoco and openai's spinning up installed

 ```bash
 python3 results/data/play.py
 ```

### Video of Cartpole

[![](http://img.youtube.com/vi/cfjH7iqALcA/0.jpg)](http://www.youtube.com/watch?v=cfjH7iqALcA "Half Cheetah")

### Video of Half Cheetah

 [![](http://img.youtube.com/vi/5H1ILqnsaHs/0.jpg)](http://www.youtube.com/watch?v=5H1ILqnsaHs "Half Cheetah")
