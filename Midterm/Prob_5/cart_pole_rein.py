# Install packages
import gym as raymond
import torch
import time
from Midterm.Prob_5.CartPoleDQN import CartPoleDQN

def main(args=None):
    env = raymond.envs.make("CartPole-v1")
    n_state = env.observation_space.shape[0]
    n_action = env.action_space.n
    episodes = 200
    n_hidden = 50
    lr = .001

    DQN = CartPoleDQN(n_state, n_action, n_hidden, lr)
    simple = CartPoleDQN.q_learning(env, DQN, episodes, gamma=.9, epsilon=.3, replay=True,verbose=False)

    env = raymond.envs.make("CartPole-v1", render_mode = "human")
    done = False
    state = env.reset()[0]
    total = 0
    while not done:
        env.render()
        q_values = DQN.predict(state)
        action = torch.argmax(q_values).item()

        next_state, reward, done, info, _ = env.step(action)

        state = next_state
        total += reward
        time.sleep(1/40)

    print("total reward: {}".format(total))


                



        

        



if __name__ == "__main__":
    main()