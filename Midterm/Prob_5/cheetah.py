from spinup import ppo_pytorch
import math
import torch
from gym.envs.mujoco.half_cheetah_v3 import HalfCheetahEnv as Raymond


class Cheetah_Raymond(Raymond):
    def __init__(self):
        super().__init__()

    def step(self, action):
        state, reward, done, info = super().step(action)
        observation = self._get_obs()
        hip_motors = (observation[3], observation[6], observation[12], observation[15])
        for motor_angle in hip_motors:
            if abs(motor_angle) > math.pi /2:
                reward -= 1
         
        return state, reward, done, info


if __name__ == '__main__':
    env_fn = lambda: Cheetah_Raymond()
    ac_kwargs = dict(hidden_sizes=[64,64], activation=torch.nn.ReLU)

    logger_kwargs = dict(output_dir='results/exp_1', exp_name  ='exp_1')

    ppo_pytorch(env_fn=env_fn, ac_kwargs=ac_kwargs, steps_per_epoch=5000, epochs=1000, logger_kwargs=logger_kwargs)


