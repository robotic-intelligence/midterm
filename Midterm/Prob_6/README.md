# Problem 6

## Part A

### Run Instructions

- Run the following command within this folder

  - ```bash
    python3 emotions.py "name of video file"
    
    # Example
    python3 emotions.py "Video_Two.mp4"
    ```

![Video One Output](output/videoOneOutput.png)
![Video Two Output](output/videoTwoOutput.png)

## Part B

- Run the following command within this folder

  - ```bash
    python3 liveEmotions.py
    ```

[![](http://img.youtube.com/vi/0iO2deYSYXI/0.jpg)](http://www.youtube.com/watch?v=0iO2deYSYXI "Live Feed")

liveEmotion_2.py is our original attempt at reading video feeds live, it has an issue process frames two slowly and doesn't add frames fast enough. 

## Part C

    Delivery robots can see if customers are satisfied, threat detection, and photo capture detection. 
    Is it ethical to categorize people based off of their emotions, and determine they are a threat. 
    If people can realize they can get a discount if customers make a unsatisfied face after delivery.


## Part D 

    Instructions to run:
    
    - Record video of two people making faces. 
    - Save the video to content and run 

    - ```bash
    python3 emotions.py "name of video file"
    ```
[![](http://img.youtube.com/vi/3Oy5Y-hOfqM/0.jpg)](http://www.youtube.com/watch?v=3Oy5Y-hOfqM "Two Faces")