from multiprocessing.connection import wait
from fer import Video
from fer import FER
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt 
import cv2
from keras.models import model_from_json

# credit to https://github.com/mayurmadnani/fer

class FacialExpressionModel(object):
    EMOTIONS_LIST = ["Angry", "Disgust", "Fear", "Happy", "Sad", "Surprise", "Neutral"]

    def __init__(self, model_json_file, model_weights_file):
        with open(model_json_file, "r") as json_file:
            loaded_model_json = json_file.read()
            self.loaded_model = model_from_json(loaded_model_json)

        self.loaded_model.load_weights(model_weights_file)
        print("Model loaded from disk")
        self.loaded_model.summary()

    def predict_emotion(self, img):
        self.preds = self.loaded_model.predict(img)
        return FacialExpressionModel.EMOTIONS_LIST[np.argmax(self.preds)]


# Put in the location of the video file that has to be processed
cap = cv2.VideoCapture(1)
if not cap.isOpened():
    cap = cv2.VideoCapture(0)
if not cap.isOpened():
    print("HOUSTON THERE IS A PROBLEM")

faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
model = FacialExpressionModel("model.json", "weights.h5")
font = cv2.FONT_HERSHEY_SIMPLEX

try:   
    while True: 
        _, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = faceCascade.detectMultiScale(gray, 1.3, 5)
        #frame = cv2.resize(frame, None, fx=0.4, fy=0.4, interpolation=cv2.INTER_AREA)
        
        for (x, y, w, h) in faces:
            fc = gray[y:y + h, x:x + w]
            roi = cv2. resize(fc, (48, 48))
            pred = model.predict_emotion(roi[np.newaxis, :, :, np.newaxis])
            cv2.putText(frame, pred, (x, y), font, 1, (255, 255, 0), 1)
            cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 1)

        if cv2.waitKey(1) == 27:
            break
        cv2.imshow('Facial Emotion Recognition', frame)
except KeyboardInterrupt as KI:
    cap.release()
    cv2.destroyAllWindows()
    sys.exit(0)



