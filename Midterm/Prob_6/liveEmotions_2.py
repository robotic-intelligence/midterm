from multiprocessing.connection import wait
from fer import Video
from fer import FER
import os
import sys
from numpy import True_
import pandas as pd
import matplotlib.pyplot as plt 
import cv2

# Put in the location of the video file that has to be processed
cap = cv2.VideoCapture(0)
while not cap.isOpened():
    print("HOUSTON THERE IS A PROBLEM")
counter = 0

try:   
    while True: 
        ret, frame = cap.read()
        frame = cv2.resize(frame, None, fx=0.4, fy=0.4, interpolation=cv2.INTER_AREA)
        print(f"frame is {frame} and type {type(frame)}")
        emo_detector = FER(mtcnn=True)
        # Capture all the emotions on the image
        captured_emotions = emo_detector.detect_emotions(frame)
        while(counter < 1000):
            counter += 1
        bounding_box = captured_emotions[0]["box"]
        emotions = captured_emotions[0]["emotions"]
        
        cv2.rectangle(frame,(
            bounding_box[0], bounding_box[1]),(
            bounding_box[0] + bounding_box[2], bounding_box[1] + bounding_box[3]),
                    (0, 155, 255), 2,)

        motion_name, score = emo_detector.top_emotion(frame)
        for index, (emotion_name, score) in enumerate(emotions.items()):
            color = (211, 211,211) if score < 0.01 else (255, 0, 0)
            emotion_score = "{}: {}".format(emotion_name, "{:.2f}".format(score))


            cv2.putText(frame, emotion_score,
                (bounding_box[0], bounding_box[1] + bounding_box[3] + 30 + index * 15),
                cv2.FONT_HERSHEY_SIMPLEX,0.5,color,1,cv2.LINE_AA,)
 
        #Save the result in new image file
        cv2.imwrite("emotion.jpg", frame)
                # Print all captured emotions with the image
        print(captured_emotions)
        cv2.imshow("Input", frame)
        c = cv2.waitKey(1)

        # Use the top Emotion() function to call for the dominant emotion in the image
        dominant_emotion, emotion_score = emo_detector.top_emotion(frame)
        print(dominant_emotion, emotion_score)
except KeyboardInterrupt as KI:
    cap.release()
    cv2.destroyAllWindows()
    sys.exit(0)



