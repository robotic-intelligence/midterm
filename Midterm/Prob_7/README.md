# Problem 7

## Run Instructions

- Run the following command within this folder

  - ```bash
    python3 name_of_algorithm.py
    # for example 
    python3 A_Star.py
    ```

## Part A

| Algorithm     |  Time | Cost  |
| ---------     | ----- | ----- |
| A-Star        | 3.754 | 63.6  |
| A-Star Own    | 6.726 | 98.4  |
| Bi-A-Star     | 3.328 | 85.2  |
| Breadth First | 6.569 | 63.6  |
| Dijkstra      | 6.638 | 63.6  |
| RRT_Start     | 272.4 | 154.4 |

- The planner with the lowest cost is a tie between A_Star, Dijkstra, and Breath_First_Search. They all follow the same path as well.
- The planner with the fastest time is my own A_star algorithm, it was fastest at the cost of memory space.
- I could make the path have a better cost, it follows the same path as the other algorithms,  but the overall cost may be calculated incorrectly.  
- In actual robotics I would use A_Star due to its fast time and optimized score. It also doesn't require the memory that my memory used to increase the speed.

## Part B

- Run the following command within this folder

  - ```bash
    python3 hybrid_a_star.py
    ```

The A Star algorithm allows the "car" to travel in straight lines making impossible turns and taking the most direct path to the end. The hybrid A Star algorithm has to use the car dynamics to make turns and create paths that are possible for the car to make. The hybrid A Star algorithm also deinsentavized turning in the car to force the car to travel in straight lines when possible. The general shape of the paths are the same, with the hybrid A Star path being longer overall to account for the size of the car and the maximum turning radius of the car. The hybrid A star algorithm uses reeds shepp paths to facility smooth curves while traveling forward as suppose to a star algorithm which requires the car to stop going forward and spin on the spot. The hybrid A Star requires the vehicle kinematics to create possible turning paths to create the final path, incentivizing going straight and making continous paths. The A star algorithm only incentivizes distance, ignoring what turns the robot will have to make.

There are two solutions to Hybrid A Star one with the default size and the other with the specified size. I uploaded both because the results just use ratios for wheel base and the result seemed a little off.

## Trajectory Images

### A star

![A Star Trajectory](fig/aStarTrajectory.png)

### Hybrid A_star Default size

![Hybrid Trajectory](fig/Figure_1.png)

### Hybrid A star specified size

![Specified Size](fig/hybridastar.png)

## Videos

### A star

[![](http://img.youtube.com/vi/w9GiXevNLzs/0.jpg)](http://www.youtube.com/watch?v=w9GiXevNLzs "A star")

### Default Size Hybrid A star

[![](http://img.youtube.com/vi/DqPL2JYwMUQ/0.jpg)](http://www.youtube.com/watch?v=DqPL2JYwMUQ "Hybrid ")

### Specified Size Hybrid A star

[![](http://img.youtube.com/vi/V4PyLAwcqo8/0.jpg)](http://www.youtube.com/watch?v=V4PyLAwcqo8 "Hybrid Specified")
