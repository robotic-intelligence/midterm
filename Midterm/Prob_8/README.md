# Problem 8

Note: All of the problems must be run from the Prob_8 directory. This because the data is hard coded (I know this is a no no)

## a)

For this problem run PytorchClassification.py from inside the Prob_8 directory. The outputs look like this:

![Prob_8_a](../Figures/Problem_8_a.png)

## b)

For this problem run ImageClassification.py. If the weights file isn't there download it from here: <https://drive.google.com/open?id=1-Aam2D-fqnwecbeHwa4rtzxtNjwcDkP6>. Then place this file in the YoloClassification/weights. This uses YOLO to put bounding boxes on the crops and weeds. The output from this program looks like this:

![Prob_8_b-1](../Figures/Problem_8_b_1.png)
![Prob_8_b-2](../Figures/Problem_8_b_2.png)

This shows an example of both a weed and a crop. The main difference between these two is the time to execute yolo is a bit longer and yolo has the ability to draw bounding boxes around objects.
In addition, although it doesn't happen with the sample images, it has the possibility to identify multiple objects in the image.

## c)

To run this section, run TransferLearning.py. This is our implementation of the popular Hotdog and not Hotdog image classification algorithm from Silicon valley. The output from this file looks like this:

![Prob_8_c_1](../Figures/Problem_8_c.png)

## d)

To make this problem work navigate to the detectron2 folder of the directory.
For this I could only use this by initializing a conda environment using the environment.yml file. You can also use the following post on stack overflow
<https://stackoverflow.com/questions/60631933/install-detectron2-on-windows-10>.
Then pip install cython, and “git+<https://github.com/philferriere/cocoapi.git#egg=pycocotools&subdirectory=PythonAPI>".
Then install detectron2 and this should work. If it doesn't I don't really know what to do, most problems happen with downloading the
right version of pytorch with cuda for your gpu.

Once this is done, run the main.py within the detectron2 folder and the statistics for RetinaNet, Mask R-CNN, and Faster R-CNN. This will display the
statistics for the stop sign dataset I created.

For RetinaNet

![Prob_8_d-1](../Figures/Problem_8_d_Retina.PNG)

For Mask R-CNN

![Prob_8_d-1](../Figures/Problem_8_d_Mask.PNG)

For Faster R-CNN

![Prob_8_d-1](../Figures/Problem_8_d_Faster.PNG)

The timings for all of these models with a test picture is

Retina: .97
Mask: .53
Faster: .066

As can be seen from the timings, Retina net is the slowest because it identifies so many boxes in the image.
Mask comes in second because it creates segmentation as well as bounding boxes.
Faster comes in the fastest but is relatively inaccurate.
