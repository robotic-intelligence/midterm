from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.data import MetadataCatalog, build_detection_test_loader
from detectron2.data.datasets import register_coco_instances
from detectron2.utils.visualizer import ColorMode, Visualizer
from detectron2 import model_zoo
from detectron2.evaluation import COCOEvaluator, inference_on_dataset


import cv2
import numpy as np
import time

class Detector:
    def __init__(self, configFile, model) -> None:
        self.model_type = model
        self.cfg = get_cfg()

        self.cfg.merge_from_file(model_zoo.get_config_file(configFile))
        self.cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(configFile)



        self.cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = .7
        self.cfg.MODEL.DEVICE = "cuda"

        self.predictor = DefaultPredictor(self.cfg)



    def onImage(self, imageFilePath):
        image = cv2.imread(imageFilePath)

        start = time.time()
        predictions = self.predictor(image)
        end = time.time()

        viz = Visualizer(image[:,:,::-1], MetadataCatalog.get(self.cfg.DATASETS.TRAIN[0]), instance_mode=ColorMode.SEGMENTATION)

        output = viz.draw_instance_predictions(predictions["instances"].to("cpu"))

        cv2.imshow("Result", output.get_image()[:,:,::-1])
        cv2.waitKey(0)
        print(f"{self.model_type} took {end-start} to finish")


    def createMetrics(self, imageFilePath, jsonPath):
        testName = "stop_sign_test" + "_" + self.model_type
        register_coco_instances(testName, {}, jsonPath, imageFilePath)
        evaluator = COCOEvaluator(testName, self.cfg, False, output_dir="Result_" + self.model_type)
        val_loader = build_detection_test_loader(self.cfg, testName)

        inference_on_dataset(self.predictor.model, val_loader, evaluator)




