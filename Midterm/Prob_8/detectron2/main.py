
from Detector import *



if __name__ == "__main__":
    RetinaNET = "COCO-Detection/retinanet_R_50_FPN_1x.yaml"
    MASK_RCNN = "COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_1x.yaml"
    Faster_RCNN = 'COCO-Detection/faster_rcnn_R_50_FPN_1x.yaml'
    
    configList = [RetinaNET, MASK_RCNN, Faster_RCNN]
    configNames = ["RetinaNet", "Mask R-CNN", "Faster R-CNN"]
    i = 0
    for configFile in configList:
        detector = Detector(configFile, configNames[i])
        detector.onImage("StopSigns/test/stop_sign/01MB90O9N0J2.jpg")
        detector.createMetrics("StopSigns/test/stop_sign/", "StopSigns/test/Stop_signs-1.json")
        i += 1