# Problem 9
## **(a)**


We believe there are a few pros to this line of thinking, however, the cons outweigh the pros. The merits of not considering ethical problems for future robotics development include quicker advances and less political hang-up from policymakers. If ethical considerations are not needed then the progress of robotics would then be unhindered by laws and rules preventing certain types of developments, especially in the realm of A.I. within robotics. 

However, the demerits of not thinking about robotics in an ethical way outweigh the merits. As robotics progress and robots become more intelligent, the opportunity for an apocalyptic event to take place increases without certain rules and regulations preventing it. If there are no boundaries for where robotics cannot go, then who is to stop a company or person from leveraging robotics to advance their own agendas? In addition, where do you draw the line between helpful robots that help humanity, whether that be an at-home assistant or in Medicare, and robotics for military purposes? Should robots be allowed to be active soldiers in the field or should we keep robotics out of war? Without considering the roboethics, you are not considering the interaction between humans and machines which is much different from just artificial intelligence which is purely software. 

Another major implication of the progression of robotics is robots taking jobs from people. We need to think through what will happen to those in jobs that could be replaced by automation and robotics. How will these people find new jobs or how will they be able to provide for their families? Robotics requires a deep dive into the socioeconomic impacts as more and more jobs are replaced by robotics.


## **(b)**


The three laws of robotics are; first: a robot may not harm a human being or through inaction allow a human being to come to harm, second: A robot must obey orders given to it by a human being except where such orders conflict with the first law, Third: A robot must protect its own existence as long as such existence does not conflict with the first or second law. These laws are quite complicated because there is much more than meets the eye. The edge cases of each situation that could lead to harm to a human or harm to the robot become quite complicated and interconnected. Our group thinks the algorithmic complexity would be quite intense as it would require very heavy testing to ensure the safety of humans. In addition, there is more than just the algorithmic complexity, you have to consider the number of sensors and actuators to even determine whether a human or robot is in danger in the first place. The ability to parse the sensor data and be able to make split-second decisions for the safety of a human is incredibly complex. The algorithm would need to compare and contrast many decisions in a very short time period to produce a decision matrix that would allow it to make the best decision. 


### **Scenario:**

If you commanded a robot to explore near a cliff edge, the robot should listen and explore near the cliff. However, the robot should not go near enough the cliff edge to put itself in danger. If at the same time the human gets dangerously close to the edge the robot should stop its command and go help the human to get them out of danger immediately. If after this is over the human commands the robot to walk off the cliff the robot must obey the human as its own self-preservation is lower on the priorities and as long as the command does not harm a human the robot must complete the task. 


### **Pseudocode:**


```
if(human_status == danger){

	helpHuman()

}

if(robot_action == (human_status=danger)){

	stopAction()

}

elseif(robot_command and !(human_status = danger)){

	doCommand()

}

elseif(robot_status == danger  and !(human_status = danger)){

	preventRobotDanger()

}
```



## **(c)**


There is currently no answer to the question of who is liable in the event of an autonomous system causing harm or damage to an individual or property. The first article speaks about a case in Arizona in which someone was fatally killed by an autonomous vehicle. The article does not go into much detail about who was found to be at fault but mentions how there are many vague and unanswered questions about who is liable in such a situation. 

The Wikipedia article speaks in depth about the different situations in which consumers or manufacturers would be responsible for an autonomous vehicle wreck. This is such a difficult problem because we currently have level 2 and 3 autonomous systems that require human drivers to be backups in case of an emergency. However, companies like Waymo are moving into the realm of level 4 and 5 autonomy which will require lawmakers to determine fault in the event of an accident. One of the interesting things that we read about is other countries' stances on liability. For example, Great Britain determines the fault based on if the person who engaged the autonomous system has insurance or not. If the person has insurance then they are liable for the damages and their insurance must take care of it. If the person is not insured then the liability falls to the owner of the vehicle. However, in Germany, the manufacturer is liable for an incident involving a level 3 autonomous system or higher.


## **(d)**


The laws that would be helpful for regulating and controlling autonomous systems would be a law similar to what we have in place for other products. If the fault of an autonomous system incident is found to be a manufacturing defect or other situation that is caused by poor engineering design then the fault should be on the manufacturer. However, if the consumer is misusing the product in any way or using the product outside of specified manufacturer limitations then the fault should lie with the operator or owner of the device. There are drawbacks to this though as it may prevent us from ever receiving fully autonomous systems as manufacturers would feel their responsibility is too great to even put effort into such an effort. In addition, manufacturers may find legal ways out of a situation by ensuring their terms of use include forgoing their liability of fault for harm caused by an autonomous system. It is a slippery slope that will be interesting to see how it is navigated in the near future.
