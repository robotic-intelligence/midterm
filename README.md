# Midterm

## The midterm is setup as a package to install

1) Create a virtual environment

```bash
python3 -m venv venv
```

2) Activate the virtual environment
In linux

```bash
source venv/bin/activate
```

In windows

```bash
source venv/Scripts/activate
```

3) In the root of the package run the following command

```bash
pip install -e .
```

4) the programs can now be run

## For all solutions to be displayed OpenAI's Spinning Up and Crop Weed detection must be installed

## Each problem has a README.md file with the solution and run instructions
