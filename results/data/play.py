from spinup.utils.test_policy import load_policy_and_env, run_policy
from Midterm.Prob_5.cheetah import Cheetah_Raymond
import os
print(os.getcwd() )
if __name__ == '__main__':
    _, get_action = load_policy_and_env('results/exp_1')
    env = Cheetah_Raymond()
    run_policy(env, get_action)