"""setup.py install python code."""
import os

from setuptools import find_namespace_packages, find_packages, setup

thelibFolder = os.path.dirname(os.path.realpath(__file__))
requirementPath = thelibFolder + "/requirements.txt"

install_requires = []
if os.path.isfile(requirementPath):
    with open(requirementPath) as f:
        install_requires = f.read().splitlines()

setup(
    name="Midterm",
    install_requires=install_requires,
    packages=find_namespace_packages(include=["Midterm.*"]),
)
